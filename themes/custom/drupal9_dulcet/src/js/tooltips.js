/* Activate Bootstrap tooltips & popovers */
/* https://getbootstrap.com/docs/4.1/components/tooltips/ */
/* https://getbootstrap.com/docs/4.1/components/popovers/ */

jQuery(document).ready(function ($) {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
});
