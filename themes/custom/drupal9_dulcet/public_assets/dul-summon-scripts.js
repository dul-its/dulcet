/* Call multiple JS files for use with the DUL Summon instance */

function include(file) {
 
    let script = document.createElement('script');
    script.src = file;
    script.type = 'text/javascript';
    script.defer = true;
 
    document.getElementsByTagName('head').item(0).appendChild(script);
 
}
 
/* Include LibKey JS file */
include(
'https://library.duke.edu/themes/custom/drupal9_dulcet/public_assets/libkey-integration.js');
/* Include Matomo JS file */
include(
'https://library.duke.edu/themes/custom/drupal9_dulcet/public_assets/matomo-code-for-summon.js');