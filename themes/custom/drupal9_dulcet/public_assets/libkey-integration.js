/* JS file for use in implementing the LibKey API from Third Iron on Summon */

var browzine = {
  libraryId: "229",
  apiKey: "277ee421-3384-4b23-9684-8664fb20e5b3",

  version: 2, // Required for the other parameters below to work after December 15, 2021.

  articlePDFDownloadLinkEnabled: true,
  articlePDFDownloadWording: "Article",
  articlePDFDownloadLinkText: "View Article PDF",

  articleLinkEnabled: true,
  articleLinkTextWording: "Article Link",
  articleLinkText: "Read Article",

  articleWording: "View Complete Issue",
  articleBrowZineWebLinkText: "Browse Now",

  journalWording: "View the Journal",
  journalBrowZineWebLinkText: "Browse Now",

  printRecordsIntegrationEnabled: true,

  unpaywallEmailAddressKey: "thomas.crichlow@duke.edu",

  articlePDFDownloadViaUnpaywallEnabled: true,
  articlePDFDownloadViaUnpaywallWording: "Article PDF",
  articlePDFDownloadViaUnpaywallLinkText: "Download Now (via Unpaywall)",

  articleLinkViaUnpaywallEnabled: true,
  articleLinkViaUnpaywallWording: "Article Link",
  articleLinkViaUnpaywallLinkText: "Read Article (via Unpaywall)",

  articleAcceptedManuscriptPDFViaUnpaywallEnabled: true,
  articleAcceptedManuscriptPDFViaUnpaywallWording: "Article PDF",
  articleAcceptedManuscriptPDFViaUnpaywallLinkText: "Download Now (Accepted Manuscript via Unpaywall)",

  articleAcceptedManuscriptArticleLinkViaUnpaywallEnabled: true,
  articleAcceptedManuscriptArticleLinkViaUnpaywallWording: "Article Link",
  articleAcceptedManuscriptArticleLinkViaUnpaywallLinkText: "Read Article (Accepted Manuscript via Unpaywall)",

  articleRetractionWatchEnabled: true,
  articleRetractionWatchTextWording: "Retracted Article",
  articleRetractionWatchText: "More Info",
};

browzine.script = document.createElement("script");
browzine.script.src = "https://s3.amazonaws.com/browzine-adapters/summon/browzine-summon-adapter.js";
document.head.appendChild(browzine.script);