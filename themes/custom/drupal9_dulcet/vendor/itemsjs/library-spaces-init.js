/* Gets data from spacefinder ap, displays it using ItemsJS */

// load external data

var apiURL = 'https://library.duke.edu/space-finder/api';
var fallbackURL = '/themes/custom/drupal9_dulcet/vendor/itemsjs/static_api.js';

function loadScript(src) {
  return new Promise(function(resolve, reject) {
    let script = document.createElement('script');
    script.src = src;

    script.onload = () => resolve(script);
    script.onerror = () => reject(new Error("Script load error: " + src));

    document.head.appendChild(script);
  });
}

let promise = loadScript(apiURL);

promise.then(

  function(result) { 
    showContent();
  },

  function(error) {  
    var imported = document.createElement('script');
    imported.src = fallbackURL;
    document.head.appendChild(imported);
    showContent();
  }

);

function showContent() {
  (function ($) {
      $("#spinner-wrapper").hide();
      $("#el").show();
      $('body').tooltip({ selector: '[data-toggle="tooltip"]'});
  }(jQuery));   
}


// https://github.com/itemsapi/itemsjs
window.onload=function(){
      
  var configuration = {
    searchableFields: ['name', 'description', 'library', 'attributes', 'location', 'amenity_ids'],
    sortings: {
      featured_asc: {
        field: ['featured', 'name'],
        order: ['desc', 'asc']
      },
    },
    aggregations: {

      noise_level: {
        title: 'Noise Level',
        conjunction: false,
        size: 20,
      },

      amenity_ids: {
        title: 'Features',
        conjunction: false,
        size: 20,
      },

      library: {
        title: 'Library',
        conjunction: false,
        size: 20,
        //sort: 'term',
        //order: 'asc',
      },

    }

  }

  itemsjs = itemsjs(rows, configuration);

  var vm = new Vue({
    el: '#el',
    data: function () {

      // making it more generic
      var filters = {};
      Object.keys(configuration.aggregations).map(function(v) {
        filters[v] = [];
      })

      return {
        query: '',
        // initializing filters with empty arrays
        filters: filters,
      }
    },
    methods: {
      reset: function () {
        var filters = {};
        Object.keys(configuration.aggregations).map(function(v) {
          filters[v] = [];
        })

        this.filters = filters;
        this.query = '';
      }
    },
    computed: {
      searchResult: function () {

        var result = itemsjs.search({
          query: this.query,
          sort: 'featured_asc',
          filters: this.filters,
          per_page: 100,
          filter: function(item) {
            return item.display == 'true';
          }
          // prefilter: function(items) {
          //   return items.filter(item => {
          //     return item.display == 'true';
          //   });
          // }

        })
        return result
      }
    }
  });
}
