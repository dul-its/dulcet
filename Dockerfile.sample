FROM bitnami/drupal:9.3.9
LABEL maintainer "Derrek Croney <derrek.croney@duke.edu>"

## Change user to perform privileged actions
USER 0

## Install 'vim'
## 'install_packages' is a "bitnami" utility
RUN install_packages vim
RUN install_packages wget
RUN install_packages rsync
RUN install_packages tar

# Because of an issue where 'updatedb' is stuck
# when running a non-interactive container, 
# we're going to copy our modified version to make 
# sure the operation assumes yes
COPY overrides/libdrupal.sh /opt/bitnami/scripts/libdrupal.sh
ENV FLAG_ASSUME_DRUSH_YES "-y"

RUN cd /tmp
RUN apt update && apt-get install -y net-tools libssl-dev libxml-xpath-perl wait-for-it

# Added this to get new enough version of Node.js to run NPM correctly
# https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs

## Install packages for compiling Drupal themes
# comment out npm here, maybe node now comes with it correctly
#RUN install_packages npm
RUN npm install -g yarn cross-env

WORKDIR /opt/dul
RUN chmod a+rwx /opt/dul

WORKDIR /opt/bitnami/drupal
RUN composer require drupal/components:~2.0
RUN composer require drupal/radix:4.11
RUN composer require drupal/samlauth:~3.0
RUN composer require drupal/pathauto:~1.0

# Config management modules
RUN composer require drupal/config_direct_save
RUN composer require drupal/config_ignore:~2.0

# Asset management modules
RUN composer require drupal/asset_injector:~2.0

# Layout & media modules
RUN composer require drupal/views_bootstrap:4.3
RUN composer require drupal/exclude_node_title
RUN composer require drupal/flexslider

# SEO Modules
RUN composer require drupal/metatag:~1.19
RUN composer require drupal/simple_sitemap:~4.1

# Stats & Analytics
RUN composer require drupal/google_analytics:~4.0

# CK Editor Custom Config 
RUN composer require drupal/ckeditor_config

# JSON Views module
RUN composer require drupal/views_json_source

# Image URL Formatter (provides path to images in views)
RUN composer require drupal/image_url_formatter:~1.0

# Add current page to breadcrumb
RUN composer require drupal/current_page_crumb:~1.3

# Copy our specific custom themes and modules.
COPY modules/ /opt/bitnami/drupal/modules/
COPY themes/ /opt/bitnami/drupal/themes/

# copy the masthead folder (represented as a submodule)
# into the container.
#
# The apache configuration will map "/masthead" to this location
COPY dul-masthead /opt/dul/masthead

# This command may be better suited in a script residing in the 
# /docker-entrypoint-init.d directory (see below)
# TODO: review
# WORKDIR /opt/bitnami/drupal/themes/custom/drupal9_dulcet
# RUN yarn && yarn dev

RUN chmod -R a+rwx /opt/bitnami/drupal/modules \
      && chmod -R a+rwx /opt/bitnami/drupal/themes

# We need to use a consistent site UUID else configuration
# import will fail. Each instance of the site gets assigned
# a new UUID, yet configs with different UUIDs are incompatible.
# See config/system.site.yml or run:
# drush cget system.site
ENV SYSTEM_SITE_UUID "90f7074a-8b32-4ddb-bd1a-31a29470d0b5"

# Copy all our config YML files from our code repo to
# a location from which they'll be imported during
# setup (see drush-tasks.sh)

ENV DUL_DRUPAL_CONFIG_DIR "/opt/dul/config"

# bitnami/drupal image offers a way to enable additional Drupal modules
# For local environments, we don't need to enable 'samlauth' by default.
# If you want to test it, however -- add 'samlauth' to this list,
ENV DRUPAL_ENABLE_MODULES "dul_ingest"

# Place cert and key for SAML SP
# We'll use this location when configuring the 
# 'samlauth' module in Drupal
WORKDIR /sp

# COPY sp.pem ./
# COPY sp.key ./
# COPY duke-metadata-2-signed.xml ./
# RUN chmod a+r /sp/sp.key
# RUN ls /sp

# Copy Drupal-related post-init scripts into the image
WORKDIR /docker-entrypoint-init.d
COPY docker-entrypoint-init.d ./

WORKDIR /opt/bitnami/drupal

# Revert back to non-root user
USER 1001

## Enable mod_ratelimit module
RUN sed -i -r 's/#LoadModule ratelimit_module/LoadModule ratelimit_module/' /opt/bitnami/apache/conf/httpd.conf

# Set the shell to "bash"
ENV SHELL /bin/bash
