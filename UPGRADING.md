# Upgrading DUL Website to Drupal 10

## Dockerfile Changes
```Dockerfile
RUN composer require 'drupal/color'
RUN composer require 'drupal/quickedit'
RUN composer require 'drupal/rdf'
```  
  
### NOTES
These three(3) modules were removed from Drupal 10 core, so I needed to 
install them in order to successfully complete an upgrade cycle.  
  
It's like these won't be needed when the website move is completed.

## Module Updates
* `drupal/components:~3.0@beta`
* `drupal/radix:~5.0`

## Module(s) Removed
* `dul_ingest` (will remove from the project since we won't be migrating data)
  
## Technical Considerations

### FlexSider Module
Currently, there is no beta or production candidate for this module, and (as always) I'm suspicious of running "alpha" branches in production. We'll need to decide on a best-course-of-action.

### Theme
#### Version Dependency Update
I did update the `core_version_requirement` to include Drupal 10.  
  
#### Considerations / Questions for Team
Do we upgrade `drupal9_dulcet` (with its heavy dependency on `radix`), or do we consider adapting Zeke's new `vivid_theme` (used for the Transparency site)?

## `docker-entrypoint-init.d` Changes
Coming soon.
