FROM bitnami/drupal:10
LABEL maintainer "Derrek Croney <derrek.croney@duke.edu>"

## Change user to perform privileged actions
USER 0

## Install 'vim' and other convienence utilities
## 'install_packages' is a "bitnami" utility
RUN install_packages vim
RUN install_packages wget
RUN install_packages rsync
RUN install_packages tar
RUN install_packages npm
RUN install_packages patch
RUN install_packages git

ENV SAMLAUTH_IDP_DIR '/opt/dul/idp'
WORKDIR /opt/dul/idp
RUN wget -O duke-metadata-2-signed.xml https://shib.oit.duke.edu/duke-metadata-2-signed.xml

# Because of an issue where 'updatedb' is stuck
# when running a non-interactive container, 
# we're going to copy our modified version to make 
# sure the operation assumes yes

#COPY overrides/libdrupal.sh /opt/bitnami/scripts/libdrupal.sh
# ENV FLAG_ASSUME_DRUSH_YES "-y"

RUN cd /tmp
RUN apt update && apt-get install -y net-tools libssl-dev libxml-xpath-perl wait-for-it curl

# Get new enough version of Node.js to run NPM correctly
# https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
#RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
#RUN apt-get install -y nodejs
RUN npm install -g yarn cross-env
RUN mkdir /.yarn
RUN touch /.yarnrc
RUN chmod a+rw /.yarnrc /.yarn

WORKDIR /opt/dul
RUN chmod a+rwx /opt/dul

WORKDIR /opt/bitnami/drupal

# copy the masthead folder (represented as a submodule)
# into the container.
#
# The apache configuration will map "/masthead" to this location
RUN mkdir /opt/dul/masthead
RUN git clone https://gitlab.oit.duke.edu/dul-its/dul-masthead /opt/dul/masthead
# COPY dul-masthead/ /opt/dul/masthead/

RUN mkdir -p /dul/media/cdvs/data \
      && chmod a+r /dul/media/cdvs/data

RUN mkdir -p /.composer/cache/files \
      && chmod -R a+rw /.composer/cache/files

# Create this directory for 'composer' use and grant it full read-write
RUN mkdir -p /.composer/cache/vcs \
  && chmod -R a+rw /.composer/cache

# We need to use a consistent site UUID else configuration
# import will fail. Each instance of the site gets assigned
# a new UUID, yet configs with different UUIDs are incompatible.
# See config/system.site.yml or run:
# drush cget system.site
ENV SYSTEM_SITE_UUID "90f7074a-8b32-4ddb-bd1a-31a29470d0b5"

# Copy all our config YML files from our code repo to
# a location from which they'll be imported during
# setup (see drush-tasks.sh)
ENV DUL_DRUPAL_CONFIG_DIR "/opt/dul/config"

# post-install tasks
COPY docker-entrypoint-init.d/ /docker-entrypoint-init.d/
RUN chmod -R +x /docker-entrypoint-init.d/*.sh

RUN mkdir -p /dul/modules/patches \
      && chmod a+rw /dul/modules/patches

WORKDIR /opt/bitnami/drupal
RUN chmod -R a+rw /opt/bitnami/drupal

ADD uid_entrypoint /bin/
RUN chmod a+x /bin/uid_entrypoint \
      && touch /etc/passwd \
      && chmod g=u /etc/passwd

# Revert back to non-root user

USER 1001

RUN composer config repositories.dul-drupal/drupal9_dulcet git \
      https://gitlab.oit.duke.edu/dul-drupal/drupal9_dulcet.git
USER 0

USER 1001

RUN composer require 'drupal/seven:^1.0'
RUN composer require 'drupal/color'
RUN composer require 'drupal/quickedit'
RUN composer require 'drupal/rdf'
RUN composer require 'drupal/components:~3.0@beta'
RUN composer require 'drupal/radix:~5.0'
RUN composer require 'drupal/samlauth'
RUN composer require 'drupal/pathauto:~1.0'
RUN composer require 'drupal/config_direct_save'
RUN composer require 'drupal/config_ignore:~2.0'
RUN composer require 'drupal/asset_injector:~2.0'
RUN composer require 'drupal/views_bootstrap:~5.4'
RUN composer require 'drupal/exclude_node_title'
RUN composer require 'drupal/flexslider:~3.0@alpha'
RUN composer require 'drupal/metatag:^1.26'
RUN composer require 'drupal/simple_sitemap:~4.1'
RUN composer require 'drupal/google_analytics:~4.0'
RUN composer require 'drupal/ckeditor_config'
RUN composer require 'drupal/views_json_source:~1.0'
RUN composer require 'drupal/image_url_formatter:~1.0'
RUN composer require 'drupal/current_page_crumb:~1.0'
RUN composer require 'drupal/bootstrap4_modal:~2.1'
RUN composer require 'drupal/redirect:~1.0'
RUN composer require 'dul-drupal/drupal9_dulcet:dev-main'

USER 0
RUN chmod -R a+rw /opt/bitnami/drupal/themes \
      && chmod -R a+rw /opt/bitnami/drupal/modules

# Adjust permissions under composer's vcs cache for drupal9_dulcet
# with the hopes that this will no longer block a manual 
# attempt to upgrade Drupal using "composer"
RUN chmod -R a+rw /.composer

USER 1001

## Apache config modifications...
## Enable modules  mod_ratelimit, mod_authnz_ldap, mod_ldap
RUN sed -i -r 's/#LoadModule ratelimit_module/LoadModule ratelimit_module/' /opt/bitnami/apache/conf/httpd.conf
RUN sed -i -r 's/#LoadModule authnz_ldap_module/LoadModule authnz_ldap_module/' /opt/bitnami/apache/conf/httpd.conf
RUN sed -i -r 's/#LoadModule ldap_module/LoadModule ldap_module/' /opt/bitnami/apache/conf/httpd.conf
RUN sed -i -r 's/CustomLog "logs\/access_log" common/#CustomLog "logs\/access_log" common/' /opt/bitnami/apache/conf/httpd.conf
RUN sed -i -r 's/#CustomLog "logs\/access_log" combined/CustomLog "logs\/access_log" combined/' /opt/bitnami/apache/conf/httpd.conf

# Set the shell to "bash"
ENV SHELL /bin/bash
