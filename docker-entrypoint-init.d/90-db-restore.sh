#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

file_name=$(basename "$0")

if [ -v SKIP_DATA_REFRESH ] && [[ "$SKIP_DATA_REFRESH" =~ ^(yes|YES) ]]; then
  info "[$file_name] skipping..."
  exit
fi

info "[$file_name] attemtping to restore production data..."
info "[$file_name] downloading from 'automation.lib.duke.edu'..."
curl -o /tmp/drupal-backup.sql -Ss https://automation.lib.duke.edu/mariadb-datastore/drupal_production_latest.sql
if [[ -f /tmp/drupal-backup.sql ]]; then
  info "[$file_name] data file exists. will process now..."
  mariadb -u ${DRUPAL_DATABASE_USER} -p${DRUPAL_DATABASE_PASSWORD} -D ${DRUPAL_DATABASE_NAME} -h ${DRUPAL_DATABASE_HOST} < /tmp/drupal-backup.sql
  info "[$file_name] restoration... done!"
  rm /tmp/drupal-backup.sql

  info "[$file_name] we need to run 'drush updb' to process any Drupal-related table changes..."
  drush -y  updb

  info "[$file_name] done."
fi
