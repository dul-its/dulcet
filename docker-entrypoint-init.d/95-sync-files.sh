#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

file_name=$(basename "$0")

if [ -v SYNC_USER_FILES ] && [ "$SYNC_USER_FILES" == "yes" ]; then
  if [[ -e "/opt/dul/lib-assets/drupal9/files-canonical" ]]; then
    info "[$file_name] syncing user files..."
    rsync -razp --no-owner --ignore-errors --ignore-existing /opt/dul/lib-assets/drupal9/files-canonical/ /bitnami/drupal/sites/default/files/
  else
    info "[$file_name] nothing to sync..."
  fi
else
  info "[$file_name] bypassing user file sync."
fi
