#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

file_name=$(basename "$0")

drush theme:enable -y claro
drush config-set system.theme admin claro -y

if [ -v DRUPAL_SKIP_BOOTSTRAP ] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "[$file_name] skipping..."
  exit
fi

info "[$file_name] Enable Dulcet theme..."

drush en components -y
drush theme:enable radix -y

drush theme:enable drupal9_dulcet -y
drush config-set system.theme default drupal9_dulcet -y

info "[$file_name] compiling site UX assets..."
cd /bitnami/drupal/themes/contrib/drupal9_dulcet
yarn && yarn dev

info "[$file_name] done!"

