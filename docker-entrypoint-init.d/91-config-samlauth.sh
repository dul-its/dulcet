#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

file_name=$(basename "$0")

if [[ -v DRUPAL_SKIP_BOOTSTRAP ]] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "[$file_name] We're not bootstrapping, so no need to configure SAML settings..."
  exit
fi

if [[ ! -v ENABLE_SAML ]]; then
  info "[$file_name] ENABLE_SAML not set -- exiting..."
  exit
fi

if [[ -v ENABLE_SAML ]] && [[ "$ENABLE_SAML" =~ ^(no|NO) ]]; then
  info "[$file_name]  ENABLE_SAML = no, so we're exiting..."
  exit
fi

drush en samlauth -y
drush install -y samlauth_user_roles

METADATA_FILE="${SAMLAUTH_IDP_DIR:-}/duke-metadata-2-signed.xml"
if [ -f "$METADATA_FILE" ]; then
  info "[SAML] Inspecting the IdP metadata..."
  IDP_CERT=`xpath -q -e '//EntityDescriptor/IDPSSODescriptor/KeyDescriptor[1]/ds:KeyInfo/ds:X509Data/ds:X509Certificate/text()' ${METADATA_FILE}`
  IDP_ENTITYID=`xpath -s "" -p "" -q -e 'string(//EntityDescriptor/@entityID)' ${METADATA_FILE}`
  IDP_SSO_SERVICE=`xpath -s "" -p "" -q -e 'string(//EntityDescriptor/IDPSSODescriptor/SingleSignOnService[@Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"]/@Location)' ${METADATA_FILE}`

  info "[SAML] Setting IdP's EntityID to '$IDP_ENTITYID"
  drush config-set samlauth.authentication idp_entity_id $IDP_ENTITYID -y

  info "[SAML] Setting SSO Sign In Service..."
  drush config-set samlauth.authentication idp_single_sign_on_service $IDP_SSO_SERVICE -y

  info "[SAML] Setting SSO Logout Service..."
  drush config-set samlauth.authentication idp_single_log_out_service 'https://shib.oit.duke.edu/cgi-bin/logout.pl' -y

  info "[SAML] Setting IdP certificate..."
  echo [${IDP_CERT//^\n/}] | drush config-set -y --input-format=yaml samlauth.authentication idp_certs -
fi

if [[ -n "$SAMLAUTH_SP_ENTITY_ID" ]]; then
  info "[SAML] Setting SP Entity ID = [${SAMLAUTH_SP_ENTITY_ID}]..."
  drush config-set samlauth.authentication sp_entity_id $SAMLAUTH_SP_ENTITY_ID -y
fi

if [[ -n "$SAMLAUTH_SP_PRIVATE_KEY" ]]; then
  info "[SAML] Setting SP Entity ID = [${SAMLAUTH_SP_PRIVATE_KEY}]..."
  drush config-set samlauth.authentication sp_private_key "file:${SAMLAUTH_SP_PRIVATE_KEY}" -y
fi

if [[ -n "$SAMLAUTH_SP_X509_CERT" ]]; then
  info "[SAML] Setting SP Entity ID = [${SAMLAUTH_SP_X509_CERT}]..."
  drush config-set samlauth.authentication sp_x509_certificate "file:${SAMLAUTH_SP_X509_CERT}" -y
fi

