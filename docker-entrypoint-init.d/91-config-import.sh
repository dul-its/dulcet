#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ -v BYPASS_CONFIG_IMPORT ] && [[ "$BYPASS_CONFIG_IMPORT" =~ ^(yes|YES|true|TRUE) ]]; then
  info "[10-import-config] Bypassing all config imports..."
  exit
fi

info "[10-import-config] Import DUL-specific configuration..."

# Set our system site UUID to a consistent value; this
# enables configurations to be exported from developers'
# local dev environments for use in other environments.
info "Setting the system.site UUID..."
drush config-set "system.site" uuid $SYSTEM_SITE_UUID -y

# Workaround for ConfigImporterException on shortcut_set. See:
# https://www.drupal.org/forum/support/post-installation/2015-12-20/problem-during-import-configuration
# but updated from deprecated entityManager to entityTypeManager
#info "Deleting default shortcut set..."
#drush ev '\Drupal::entityTypeManager()->getStorage("shortcut_set")->load("default")->delete();'

# Import all config YML files that were copied from
# our code repo to the config directory (see Dockerfile).
# See https://drushcommands.com/drush-9x/config/config:import/
drush config-import --source $DUL_DRUPAL_CONFIG_DIR --partial -y

# If a custom GA account is specified for this env, override the value configured in
# the google_analytics module (see config/google_analytics.settings.yml).
# Note that this gets written to sites/default/settings.php.
#if [ ! -z "$GOOGLE_ANALYTICS_ID" ]; then
#  drupal_conf_set "\$config['google_analytics.settings']['account']" "$GOOGLE_ANALYTICS_ID" no
#fi

info "Done with configuration import!"
