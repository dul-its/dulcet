#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

file_name=$(basename "$0")

if [ -v DRUPAL_SKIP_BOOTSTRAP ] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "[$file_name]: skipping..."
  exit
fi
info "[$file_name] Applying patches and such..."

# Navigate to the redirect module directory
# cd /bitnami/drupal/modules/contrib/redirect
# ls -la

# Apply the redirect module patch
# info "[$file_name] Patching redirect module..."
#patch -p1 < /dul/modules/patches/redirect/redirection-not-working-3408075-3.patch

#drush cr
