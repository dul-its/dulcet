# OKD Setup for DUL Web

## Checklist
**Create Secrets for Database and Secret Keys**  
 
Manaully create the database secrets for MariaDB:  
```bash
$ kubectl create secret generic app-mariadb --from-literal=mariadb-root-password=$(openssl rand -base64 16) --from-literal=mariadb-password=$(openssl rand -base64 16)
```
  
  
```sh
helm upgrade --install prod . --set image.tag=1.2.2 --set-file saml.sp_key_file=../sp/prod-sp.key --set-file saml.sp_cert_file=../sp/prod-sp.pem -f values-prod.yaml
```
