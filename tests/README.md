# WRITING TEST SCRIPTS

## TL;DR

**_NOTE:_** Use `http://localhost:8081` when attempting to access a page inside the test container.

```bash
#!/bin/bash

# Let's say we went to verify an HTTP 200 OK header response
RESPONSE=$(curl -sS -I http://localhost:8081)
if [[ ! "$RESPONSE" =~ "200 OK" ]]; then
  echo "[my-test] Test Label: FAIL"
  exit 1
fi
echo "[my-test] Test Label: PASS"

# Be sure to exit with the non-error code (0)
exit 0
```

## `bin/run-tests`
**_Usage_**  
`$ run-tests [-p <port-number>]`

## Running Locally

### Complete Test Suite
```bash
$ cd /path/to/dulcet-root

# with your container running (likely at port 80)
$ ./bin/run-tests -p 80
```

### Individual File (for simple unit test)
```bash
$ cd /path/to/dulcet-root

# with your container running (likely at port 80)
$ ./tests/XX_section__my_test_here.sh 80
```
