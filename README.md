# Drupal-powered DUL Public Website
[[_TOC_]]
## Preamble
DUL's public website, `https://library.duke.edu`, is powered by Drupal 10.

## Prune, Prune, Prune!
```bash
$ docker system prune -af
```

## The `WIP-prep-for-D10` branch contains:
* * `docker-compose.sample.yml` --> `cp docker-compose.sample.yml docker-compose.yml` and use this one to run the D10 instance.
* **Contrib modules from our Drupal 9 instance:**
* * virtually all of the contributed modules have been copied to `modules/contrib`
* * these will be removed once we have a stable D10 environment
  
## Part 1: Drupal 9
### Download backup of DUL Drupal 9 database:
```bash
$ cd /path/to/dulcet
$ curl -o drupal9_local.sql https://automation.lib.duke.edu/mariadb-datastore/drupal9_production_latest.sql
```

### Prepare Persistence
I recommend using your own directories, instead of using Docker's volume setup - makes things easy when you need to clear everything out.

#### MariaDB
```bash
$ [sudo] mkdir -p /var/lib/dulcet/mariadb
$ [sudo] chmod -R a+rw /var/lib/dulcet/mariadb
```

#### Drupal
```bash
$ cd /path/to/dulcet
$ mkdir drupal-persist
$ chmod a+rw drupal-persist
```


## Part 2: Run Drupal 10
everything is ready to just simply run:  
  
```bash
$ docker compose up --build
```

You should now be able to visit localhost to see the running app!

## Resetting Persistence
```bash

$ docker system prune -af

$ sudo rm -rf drupal-persist/.user_scripts_initialized && sudo rm -r drupal-persist/* 

or

$ docker volume rm <name-of-drupal-persist-volume>
```
