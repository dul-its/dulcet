#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# install old admin theme
# composer require 'drupal/seven:^1.0'

# Navigate to the redirect module directory
cd /bitnami/drupal/modules/contrib/redirect

# Apply the redirect module patch
patch -p1 < /bitnami/drupal/modules/contrib/redirect/redirection-not-working-3408075-3.patch
