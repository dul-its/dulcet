# Compiling Assets (CSS & JS)
## Overview
Our Dulcet theme [uses NPM, Yarn, Webpack, and Laravel Mix](https://duldev.atlassian.net/wiki/spaces/DSTP/pages/1751384065/Deploying+CSS+and+JS) for front-end builds. Relevant configuration is in these files:
- `package.json`: defines scripted commands like `yarn dev` & `yarn watch`; specifies Node module dependencies like Bootstrap & FontAwesome.
- `webpack.mix.js`: defines configs for compiling JS & SCSS assets & loading webfonts

## Iterating Assets in a Local Environment
**1) Build**
```sh
$ docker-compose up --build
```

**2) Mount the `src` Directory**

Hit `Ctrl-C` to break after the build completes. Uncomment this line in your `docker-compose.yml` file and save it:
```yml
- './themes/custom/drupal9_dulcet/src:/bitnami/drupal/themes/custom/drupal9_dulcet/src:ro'
```
**NOTE**: if this is done _before_ step 1, the build will fail. So remember to re-comment out the line before doing a fresh rebuild.

**3) Bring the stack back up (_will be quick this time_):**
```sh
$ docker-compose up --build
```

**4) Watch**

In another terminal tab, watch for asset changes:
```
$ docker exec -u 0 -it drupal9_app /bin/bash -c 'cd /bitnami/drupal/themes/custom/drupal9_dulcet ; yarn watch'
```

**5) Edit**

Edit a `.js` or `.scss` file in the `src` directory and see a live re-compilation.

**6) Clear Cache**

In yet another terminal tab, clear the Drupal cache:
```
$ docker exec -u 0 -it drupal9_app drush cache:rebuild
```

**7) See Changes**

Visit a page on `localhost` to see the changes in place.

Repeat steps 5-7 as needed.