# Setting Up Service Provider (SP) Metadata

Visit [onelogin](https://www.samltool.com/sp_metadata.php) and enter the data for:
* EntityId
* Attribute Consume Service Entpoint (HTTP-POST)
* Single Logout Service Endpoint (HTTP-REDIRECT, optional)
* SP X.509 Certificate
  
Then click/tap the "Build SP Metadata" button located at the bottom of the page.  
  
Save this file in the dulcet's root directory.
